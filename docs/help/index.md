# About
This site is produced and managed by the [Research Computing Interns](interns.md).

Have questions or suggestions for additional documentation? Email [oitresearchsupport@duke.edu](). We can help you get started with research computing resources. For advanced topics, book office hours through [this link.](https://outlook.office365.com/owa/calendar/ResearchComputing@ProdDuke.onmicrosoft.com/bookings/)