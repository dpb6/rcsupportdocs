# Overview 
[Globus](https://www.globus.org) is a data management service frequently used in the research community to transfer or share large scale research data. It is a non-profit service run by the University of Chicago that is available to all Duke users under a standard Globus subscription.

## How do I use Globus?

If your lab already has access to a Globus node you can:

* Install and configure Globus Connect personal for [Mac OS X](https://docs.globus.org/how-to/globus-connect-personal-mac/), [Windows](https://docs.globus.org/how-to/globus-connect-personal-windows/), or [Linux](https://docs.globus.org/how-to/globus-connect-personal-linux/)
* [Log in and transfer files](https://docs.globus.org/how-to/get-started/) with Globus
* [Share data](https://docs.globus.org/how-to/share-files/) with other collaborators
* [Use the CLI](https://docs.globus.org/cli/) to automate the data transfer from scientific instruments to lab storage

If you are new to Globus and need access to an existing node, contact the node(s) IT administrator for access.

If you are an IT administrator and would like to setup a new node, contact [research computing](mailto:rescomputing@duke.edu). 

## More About Globus

Data repositories, such as your laptop, campus compute resources, scientific instruments, or archival storage, are connected to Globus as a node. Users and administrators are then able to configure permissions on the nodes so that transfers and/or sharing can be done by appropriate Globus users.

At Duke, you can use Globus to transfer or share public and restricted research data between approved endpoints. It is a common method used to move large data between:

* Scientific instruments and lab storage
* Lab storage and Duke computational resources like the Duke Compute Cluster or HARDAC
* Laptops and shared computing infrastructure
* Duke storage and collaborators at peer institutions like UNC or external computing labs

## Why use Globus? 

Globus was specifically designed for transferring and sharing research data and offers several key benefits:

**Reliability:** Data is transferred directly between nodes and Globus automatically tunes performance, maintains security, validates correctness, and recovers from errors to restart the transfer

**Unified Interface:** All of your Globus nodes are visible in a single web interface that is easy to use

**Remote Initiation:** Transfers can be initiated from your laptop between any nodes directly without data transversing your laptop

**External Collaboration:** Globus is used by many research institutions and allows users to connect with their institutional credentials to transfer data intra and inter institution­­­­­­

Globus may be used with public and restricted data at Duke.  It may not be used with sensitive data at this time.

