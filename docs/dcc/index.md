# User Guide
The Duke Compute Cluster is a general purpose high performance/high-throughput installation, and it is fitted with software used for a broad array of scientific projects. With a few notable exceptions, applications on the cluster are generally Free and Open Source Software. The DCC is managed and supported by the Office of Information Technology (OIT) and Research Computing. 

The Duke Compute Cluster consists of machines that the University has provided for community use and that researchers have purchased to conduct their research. The equipment is housed in enterprise-grade data centers on Duke’s West Campus. Cluster hardware is heterogenous based on installation date and current standards. Quick facts:

* 1300 nodes which combined have more than 30,000 vCPUs, 750 GPUs and 200TB of RAM
* Interconnects are 10 Gbps or 40 Gbps
* Utilizes a 7 Petabyte Isilon file system
* Runs CentOS 8 Stream and SLURM is the job scheduler

General users have access to common nodes purchased by the University and low-priority access to researcher purchased nodes. Researchers who have provided equipment have high-priority access to their nodes in addition to the general access. Low priority consumption of cycles greatly increases the efficiency of the cluster overall, while also providing all users the benefit of being able to access more than their own nodes’ cycles when they might need it. Low priority jobs on the machines yield to high priority jobs.

## Appropriate Use

Users of the cluster agree to only run jobs that relate to the research mission of Duke University. Use of the cluster for the following activities is prohibited:

* Financial gain
* Commercial or business use
* Unauthorized use or storage of copyright-protected or proprietary resources
* Unauthorized mining of data on or off campus (including many web scraping techniques)

## Data Security and Privacy

Users of the cluster are responsible for the data they introduce to the cluster and must follow all applicable Duke (including IRB), school, and departmental policies on data management and data use. Security and compliance provisions on the cluster are sufficient to meet the [Duke data classification standard](https://security.duke.edu/policies/data-classification-standard) for public or restricted data.  Use of sensitive data (e.g. legally protected data such as PHI or FERPA) or data bound by certain restrictions in data use agreements is not allowed. Data that has been appropriately de-identified or obfuscated potentially may be introduced to the cluster without violating data use agreements or government regulations.

As a shared resource, privacy on the cluster is constrained and users of the cluster must conduct themselves in a manner that respects other researchers’ privacy. Cluster support staff have access to all data on the cluster and may inspect elements of the system from time to time. Metadata on the cluster and utilization by group (and sometimes user) will be made available to all cluster users and Duke stakeholders.


## Getting a DCC Account
Duke researches are granted access to the DCC by the point of contact for their group or lab, if you need help finding your point of contact, check the list of [current DCC groups.](https://rtoolkits.web.duke.edu/projects/report)

Group point of contacts manage DCC membership through our self service portal: [rtoolkits.web.duke.edu](https://rtoolkits.web.duke.edu). If you are a faculty researcher and are interested in setting up a new group, either to purchase equipment for the cluster, or to try out the cluster using common and scavenger access, [contact us](../help/index.md).

Any user with a Duke netid can be added to the DCC.

## Purchasing resources for the DCC
For research groups needing access to additional computing resources, PIs may purchase compute nodes from current standard options to add to the cluster.

There are no operating costs for managing and housing PI-purchased compute nodes that are part of the standard cluster installation. Owners have priority access to the computing resources they purchase, but can access more nodes for their research if they need to.

Research groups interested in purchasing computing nodes or additional storage should email [Research Computing](mailto:oitresearchsupport@duke.edu).