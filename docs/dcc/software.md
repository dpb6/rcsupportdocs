# Software

## Software modules
Software is provided on the DCC under the form of loadable environment modules. The use of a module system means that most software is not accessible by default and has to be loaded using the module command. This mechanism allows us to provide multiple versions of the same software concurrently, and gives users the possibility to easily switch between software versions. The modules system helps setting up the user's shell environment to give access to applications, and make running and compiling software easier.

When you first login, you'll be presented with a default, bare bones environment with minimal software available. The module system is used to manage the user environment and to activate software packages on demand. In order to use software installed, you must first load the corresponding software module.

When you load a module, the system will set or modify your user environment variables to enable access to the software package provided by that module. For instance, the `$PATH` environment variable might be updated so that appropriate executables for that package can be used.

Run `module avail` to see the list of installed applications

Run `module load (module name)` to use the application

You can add the `module load` command to your job scripts, or to the end of the .bash_profile file in your home directory.

Applications can be installed on request, email [rescomputing@duke.edu](mailto:rescomputing@duke.edu). Generally, applications should be open source and recommended for use on a computing cluster.

## User installed software
Lab groups are welcome to install software in their `hpc/group/<groupname>` space if `sudo` access is not required.  This can be helpful for lab specific software or for when a group wants to strongly control software versions and packages.

### Miniconda installation sample
    mkdir -p /hpc/group/<groupname>/software
    wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    sh Miniconda3-latest-Linux-x86_64.sh

and then follow the instructions. The place to give as Miniconda install location should be `/hpc/group/<groupname>/software/miniconda3`. It will offer to update your ~/.bashrc, (init) which is what you want.   Log out log back in and then you can run conda install, pip install, create environments, etc.


 
    


