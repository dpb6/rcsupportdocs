Users must have an existing account on the Duke Compute Cluster to access DCC OnDemand.

## Login
Login to DCC OnDemand at [dcc-ondemand-01.oit.duke.edu](https://dcc-ondemand-01.oit.duke.edu)

The DCC OnDemand **Dashboard** will then open. From there, you can use the
menus across the top of the page to manage files, get a shell on the DCC,
submit jobs or open interactive applications.

To end your OnDemand session, click on the Log Out link at the top right of the Dashboard window and close your browser.

## Managing your files

To create, edit or move files, click on the **Files** menu from the
**Dashboard** page. A dropdown menu will appear, listing your home directory and the work directory on the DCC. Choosing one of the file spaces opens the **File Explorer** in a new browser tab. The files in the selected directory are listed.

You can navigate to any directory mounted on the DCC by manually entering the [full file system path](../dcc/files.md).

Once you are in the correct directory, you can transfer files to and from the DCC using the DCC OnDemand web interface.

## Working with Jobs

You can view current DCC jobs, create new job scripts, edit existing scripts, and submit them to the
scheduler throught the DCC OnDemand interface.

To access the job management tools, use the "Jobs" menu. For more information about using the tools, please visit the [OSC job management page](https://www.osc.edu/resources/online_portals/ondemand/job_management).



## Getting a shell

You can get shell access to the DCC by choosing **Clusters > DCC Shell
Access** from the top menu in the OnDemand **Dashboard**.

In the window that will open, you'll be logged in to one of the DCC's login
nodes, exactly as if you were using SSH to connect. Except you don't need to
install any SSH client on your local machine.

## Interactive applications

One of the main features of DCC OnDemand is the ability to run
interactive applications difrectly from the web interface, without leaving your
web browser.

