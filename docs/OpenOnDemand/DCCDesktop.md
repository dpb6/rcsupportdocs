# Using the DCC Desktop with Matlab

This procedure demonstrates how to load Matlab, but can be done with any applications installed on the DCC that have a GUI interface (for example: Matlab, Mathematica, Comsol).

1. Click on Interactive Apps in the top navigation menu
2. Click on DCC Desktop

## Launching a DCC Desktop
1. For lab account, input the name of your DCC group (list of all groups can be found [here](https://dcc-ondemand-01.oit.duke.edu/pun/sys/dashboard/files/fs//hpc/group))
2. Under partition, type in "common", or if your lab has dedicated resources, add your own partition
3. Input the number of hours you would like the session to remain active (please try to remain small, as it will continue running even if you are not using it)
4. Input the desired amount of nodes, memory, and CPUs (try to start small with only a few gigabytes of memory and cores)
5. Press the blue "Launch" button on the buttom of the page
6. After pressing the blue "launch" button, your job will be queued to start
7. Wait a few seconds to a few minutes for the Desktop to finish launching. The status will automatically change from "Starting" to "Running" when it is ready.  If you have requested a large number of resources, your session will wait to start until the resources are available on the DCC.
8. Press the blue "Launch DCC Desktop" button when it is ready

## Using DCC Desktop to launch matlab
In the bottom center launch a terminal window:

    $module load Matlab R2021a
    $matlab

You Matlab gui should now launch within your browser window.