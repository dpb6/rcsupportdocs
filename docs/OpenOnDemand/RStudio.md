# Using the RStudio server

1. Click on Interactive Apps in the top navigation menu
2. Click on RStudio Server

## Launching an RStudio server
1. For lab account, input the name of your DCC group (list of all groups can be found [here](https://dcc-ondemand-01.oit.duke.edu/pun/sys/dashboard/files/fs//hpc/group))
2. Under partition, type in "common"
3. Input the number of hours you would like the server to remain active (please try to remain small, as it will continue running even if you are not using it)
4. Input the desired amount of nodes, memory, and CPUs (try to start small with only a few gigabytes of memory and cores)
5. Optionallly, enter a Datacommons mount path and/or any additional Slurm parameters
6. Press the blue "Launch" button on the buttom of the page

## Connecting to RStudio 
1. After pressing the blue "launch" button, your job will be queued to start an RStudio server. You should see this automatically
2. Wait a few seconds to a few minutes for the RStudio server to finish launching. The status will automatically change from "Starting" to "Running" when the server is ready
3. Press the blue "Connect to RStudio Server" button when the server is running to access your RStudio server

## Using RStudio Server
1. In the top left of the interface, click on "File" > "New File" > "R Script" to create a new R Script
2. To save this file, use Ctrl+S (Command+S on macOS) and choose a file path
3. Alternatively, upload your existing .r files using the "Upload" button toward the top of the pane in the bottom left corner
4. When you are ready, you can run your R Script by pressing the "Run" button toward the top right of the top left pane
